#ifndef QBP_H
#define QBP_H

#include "arm_math.h"
#include "nn_params.h"
#include "backward_pass.h"
#include "q_forward_pass.h"
#include "quantization.h"

q15_t q_node_loss_pd(loss_functions loss, q15_t output_trg, q15_t model_out);

q15_t q_node_activation_pd(q15_t node_output, activation_function_type act_func);

void q_hiddenLayer_delta(float* delta_inBuff, q7_t* weights, int8_t weights_iw, const unsigned char nextLayer_dim, q7_t* currLayer_out, activation_function_type act_func, const unsigned char currLayer_dim, float* delta_outBuff);

void q_update_parameters(float* delta_inBuff, q7_t* layer_weights, uint8_t* weights_fb, q7_t* layer_bias, uint8_t* bias_fb, q7_t* prevLayer_out, const unsigned char prevLayer_dim, const unsigned char currLayer_dim, float learning_rate);

void q_back_prop(float* input_sample, float* out_trg, ann_model_q7* model_q7, float learning_rate);

void q_outputLayer_delta(float* out_target, q7_t* layer_outs, activation_function_type act_func, const unsigned char layer_size, float* delta_outBuff);


#endif