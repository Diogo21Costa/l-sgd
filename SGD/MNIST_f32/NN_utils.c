#include "NN_utils.h"


// Load dataset
int load_dataset(float **in_samples, float** out_labels, int dataset_size, int input_dim)
{
    for(int i=0;i<dataset_size;i++)
    {
        in_samples[i] = (float*)malloc(input_dim * sizeof(float));
    }

    for(int i=0;i<dataset_size;i++)
    {
        in_samples[i] = (float*)malloc(input_dim * sizeof(float));
    }

    return SUCCESS_LOAD_DATA;
}


// Init the Neural Network Architecture
int init(layer **lay, int num_layers, int* layers_dims)
{
    *lay = (layer*)malloc(num_layers * sizeof(layer));
    if(create_architecture(*lay, num_layers, layers_dims) != SUCCESS_CREATE_ARCHITECTURE)
    {
        printf("Error in creating architecture...\n");
        return ERR_INIT;
    }

    printf("Neural Network Created Successfully...\n\n");
    return SUCCESS_INIT;
}

// Create Neural Network Architecture
int create_architecture(layer *lay, int num_layers, int* layers_dims)
{
    int i=0,j=0;

    for(i=0;i<num_layers;i++)
    {
        lay[i] = create_layer(layers_dims[i]);      
        lay[i].num_neu = layers_dims[i];
        printf("Created Layer: %d\n", i+1);
        printf("Number of Neurons in Layer %d: %d\n", i+1, lay[i].num_neu);

        for(j=0;j<layers_dims[i];j++)
        {
            if(i < (num_layers-1)) 
            {
                lay[i].neu[j] = create_neuron(layers_dims[i+1]);
            }

            //printf("Neuron %d in Layer %d created\n",j+1,i+1);  
        }
        printf("\n");
    }

    printf("\n");

    // Initialize the weights
    if(initialize_weights(lay, num_layers, layers_dims) != SUCCESS_INIT_WEIGHTS)
    {
        printf("Error Initilizing weights...\n");
        return ERR_CREATE_ARCHITECTURE;
    }

    return SUCCESS_CREATE_ARCHITECTURE;
}


int initialize_weights(layer* lay, int num_layers, int* layers_dims)
{
    int i,j,k;
    int layer_idx, neuron_idx, nextLayer_neuron_idx;
    if(lay == NULL)
    {
        printf("No layers in Neural Network...\n");
        return ERR_INIT_WEIGHTS;
    }

    printf("Initializing weights...\n");


    if(LOAD_WEIGHTS)
    {
        //Layer 0
        layer_idx = 0;
        for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
        {
            //Load neuron weights
            for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
            {
                // Initialize Output Weights for each neuron
                lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx] = layer_0_wt[neuron_idx][nextLayer_neuron_idx];
                //printf("%d:w[%d][%d]: %f\n", nextLayer_neuron_idx, layer_idx, neuron_idx, lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]);
                lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx] = 0.0;
            }
        }

        //Layer 1
        layer_idx = 1;
        for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
        {
            //Load neuron weights
            for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
            {
                // Initialize Output Weights for each neuron
                lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx] = layer_1_wt[neuron_idx][nextLayer_neuron_idx];
                //printf("%d:w[%d][%d]: %f\n", nextLayer_neuron_idx, layer_idx, neuron_idx, lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]);
                lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx] = 0.0;
            }

            //Load neuron bias
            lay[layer_idx].neu[neuron_idx].bias = layer_0_bs[neuron_idx];
        }

        //Layer 2
        layer_idx = 2;
        for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
        {
            //Load neuron weights
            for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
            {
                // Initialize Output Weights for each neuron
                lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx] = layer_2_wt[neuron_idx][nextLayer_neuron_idx];
                //printf("%d:w[%d][%d]: %f\n", nextLayer_neuron_idx, layer_idx, neuron_idx, lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]);
                lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx] = 0.0;
            }

            //Load neuron bias
            lay[layer_idx].neu[neuron_idx].bias = layer_1_bs[neuron_idx];
        }

        //Layer 3
        layer_idx = 3;
        for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
        {
            //Load neuron bias
            lay[layer_idx].neu[neuron_idx].bias = layer_2_bs[neuron_idx];
        }

    }

    else
    {
        for (i = 0; i < N_LAYERS - 1; i++)
        {

            for (j = 0; j < layers_dims[i]; j++)
            {
                for (k = 0; k < layers_dims[i + 1]; k++)
                {
                    // Initialize Output Weights for each neuron
                    lay[i].neu[j].out_weights[k] = ((double)rand()) / ((double)RAND_MAX);
                    printf("%d:w[%d][%d]: %f\n", k, i, j, lay[i].neu[j].out_weights[k]);
                    lay[i].neu[j].dw[k] = 0.0;
                }

                if (i > 0)
                {
                    lay[i].neu[j].bias = ((double)rand()) / ((double)RAND_MAX);
                }
            }
        }
        printf("\n");

        for (j = 0; j < layers_dims[N_LAYERS - 1]; j++)
        {
            lay[N_LAYERS - 1].neu[j].bias = ((double)rand()) / ((double)RAND_MAX);
        }
    }  

    return SUCCESS_INIT_WEIGHTS;
}