#include "backprop.h"

void train_nn(layer* lay, int* num_neurons, int* activation_functions,
            float lrn_rate, int epochs,
            float **samples, float* labels, int n_samples)
{
    //int epoch=0;

    // Gradient Descent
    for(int epoch=0; epoch<epochs; epoch++)
    {
        printf("Running epoch %d/%d", epoch, epochs);
        for(int sample_idx=0; sample_idx<n_samples; sample_idx++)
        {
            feed_input(lay, samples, INPUT_FEATURES, sample_idx);
            forward_prop(lay, num_neurons, activation_functions);

            float sample_targets[OUTPUT_FEATURES];
            if (num_neurons[N_LAYERS - 1] > 1)
            {
                for (int i = 0; i < OUTPUT_FEATURES; i++)
                {
                    if (labels[sample_idx] == i)
                        sample_targets[i] = 1;
                    else
                        sample_targets[i] = 0;
                }
            }

            else
                sample_targets[0] = labels[sample_idx];
            
            //selecionar labels
            back_prop(lay, num_neurons, activation_functions, sample_targets, LOSS_FUNCTION);
            update_weights(lay, lrn_rate, num_neurons);
        }
    }
}


float loss_derivative(int loss_function, float out_target, float neuron_output)
{
	float loss_derivative = 0;
	float num, den;

	switch (loss_function)
	{
	case MSE:
		loss_derivative = neuron_output - out_target;
		break;

	case BCE:
		if (neuron_output == 0.0)
		{
			num = 1 - out_target;
			den = 1 - neuron_output;
		}

		else if (neuron_output == 1.0)
		{
			num = out_target;
			den = neuron_output;
		}

		else
		{
			num = neuron_output - out_target;
			den = neuron_output * (1 - neuron_output);
		}
		
		loss_derivative = num / den;
		break;

	case CE:
		if (neuron_output == 0.0)
			neuron_output = 0.0001;
		loss_derivative = out_target / neuron_output;
		break;
	}

	return loss_derivative;

}


float activation_derivative(int activation_function, float neuron_output)
{
	float activation_pd = 0;

	switch (activation_function)
	{
	case RELU:
		if (neuron_output > 0.0)
			activation_pd = 1.0;
		else
			activation_pd = 0.0;
		break;

	case SIGMOID:
		activation_pd = neuron_output * (1.0 - neuron_output);
		break;

	case TANH:
		activation_pd = 1.0 - (neuron_output * neuron_output);
		break;

	default:
		break;
	}

	return activation_pd;
}


void back_prop(layer* lay, int* layers_n_neurons, int* activation_functions, float* target, int loss_func)
{
    // Output Layer
    for(int neuron_idx=0; neuron_idx<layers_n_neurons[N_LAYERS-1]; neuron_idx++)
    {           
        float out_target = target[neuron_idx];
        float out_neuron = lay[N_LAYERS - 1].neu[neuron_idx].actv;

        float loss_pd = loss_derivative(LOSS_FUNCTION, out_target, out_neuron);
        float act_pd = activation_derivative(activation_functions[N_LAYERS-2], out_neuron);
        
        lay[N_LAYERS-1].neu[neuron_idx].dz = loss_pd * act_pd;

        for(int k=0;k<layers_n_neurons[N_LAYERS-2];k++)
        {   
            lay[N_LAYERS-2].neu[k].dw[neuron_idx] = (lay[N_LAYERS-1].neu[neuron_idx].dz * lay[N_LAYERS-2].neu[k].actv);
            lay[N_LAYERS-2].neu[k].dactv = lay[N_LAYERS-2].neu[k].out_weights[neuron_idx] * lay[N_LAYERS-1].neu[neuron_idx].dz;
        }
            
        lay[N_LAYERS-1].neu[neuron_idx].dbias = lay[N_LAYERS-1].neu[neuron_idx].dz;           
    }

    // Hidden Layerss
    for(int i=N_LAYERS-2;i>0;i--)
    {
        for(int j=0;j<layers_n_neurons[i];j++)
        {
            float out_neuron = lay[i].neu[j].actv;
            float act_pd = activation_derivative(activation_functions[i-1], out_neuron);
            lay[i].neu[j].dz = lay[i].neu[j].dactv * act_pd;

            for(int k=0;k<layers_n_neurons[i-1];k++)
            {
                lay[i-1].neu[k].dw[j] = lay[i].neu[j].dz * lay[i-1].neu[k].actv;    
                
                if(i>1)
                {
                    lay[i-1].neu[k].dactv = lay[i-1].neu[k].out_weights[j] * lay[i].neu[j].dz;
                }
            }

            lay[i].neu[j].dbias = lay[i].neu[j].dz;
        }
    }
}


void update_weights(layer* lay, float lrn_rate, int* layers_dims)
{
    int i, j, k;
    int layer_idx, neuron_idx, nextLayer_neuron_idx;

    

    //for(i=0;i<N_LAYERS-1;i++)
    //{
    //    for(j=0;j< layers_dims[i];j++)
    //    {
    //        for(k=0;k< layers_dims[i+1];k++)
    //        {
    //            // Update Weights
    //            lay[i].neu[j].out_weights[k] = (lay[i].neu[j].out_weights[k]) - (lrn_rate * lay[i].neu[j].dw[k]);
    //        }
    //        
    //        // Update Bias
    //        lay[i].neu[j].bias = lay[i].neu[j].bias - (lrn_rate * lay[i].neu[j].dbias);
    //    }
    //}

    //calculate new weights

    //Layer 0
    layer_idx = 0;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
        {
            // Update Weights
            layer_0_wt[neuron_idx][nextLayer_neuron_idx] = (lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]) - (lrn_rate * lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx]);
        }
    }

    //Layer 1
    layer_idx = 1;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
        {
            // Update Weights
            layer_1_wt[neuron_idx][nextLayer_neuron_idx] = (lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]) - (lrn_rate * lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx]);
        }

        // Update Bias
        layer_0_bs[neuron_idx] = lay[layer_idx].neu[neuron_idx].bias - (lrn_rate * lay[layer_idx].neu[neuron_idx].dbias);
    }

    //Layer 2
    layer_idx = 2;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
        {
            // Update Weights
            layer_2_wt[neuron_idx][nextLayer_neuron_idx] = (lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]) - (lrn_rate * lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx]);
        }

        // Update Bias
        layer_1_bs[neuron_idx] = lay[layer_idx].neu[neuron_idx].bias - (lrn_rate * lay[layer_idx].neu[neuron_idx].dbias);
    }

    //Layer 3
    layer_idx = 3;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        // Update Bias
        layer_2_bs[neuron_idx] = lay[layer_idx].neu[neuron_idx].bias - (lrn_rate * lay[layer_idx].neu[neuron_idx].dbias);
    }
 

    //Update model weights
    //Layer 0
    layer_idx = 0;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        //Load neuron weights
        for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
        {
            // Initialize Output Weights for each neuron
            lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx] = layer_0_wt[neuron_idx][nextLayer_neuron_idx];
            //printf("%d:w[%d][%d]: %f\n", nextLayer_neuron_idx, layer_idx, neuron_idx, lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]);
            lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx] = 0.0;
        }
    }

    //Layer 1
    layer_idx = 1;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        //Load neuron weights
        for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
        {
            // Initialize Output Weights for each neuron
            lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx] = layer_1_wt[neuron_idx][nextLayer_neuron_idx];
            //printf("%d:w[%d][%d]: %f\n", nextLayer_neuron_idx, layer_idx, neuron_idx, lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]);
            lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx] = 0.0;
        }

        //Load neuron bias
        lay[layer_idx].neu[neuron_idx].bias = layer_0_bs[neuron_idx];
    }

    //Layer 2
    layer_idx = 2;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        //Load neuron weights
        for (nextLayer_neuron_idx = 0; nextLayer_neuron_idx < layers_dims[layer_idx + 1]; nextLayer_neuron_idx++)
        {
            // Initialize Output Weights for each neuron
            lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx] = layer_2_wt[neuron_idx][nextLayer_neuron_idx];
            //printf("%d:w[%d][%d]: %f\n", nextLayer_neuron_idx, layer_idx, neuron_idx, lay[layer_idx].neu[neuron_idx].out_weights[nextLayer_neuron_idx]);
            lay[layer_idx].neu[neuron_idx].dw[nextLayer_neuron_idx] = 0.0;
        }

        //Load neuron bias
        lay[layer_idx].neu[neuron_idx].bias = layer_1_bs[neuron_idx];
    }

    //Layer 3
    layer_idx = 3;
    for (neuron_idx = 0; neuron_idx < layers_dims[layer_idx]; neuron_idx++)
    {
        //Load neuron bias
        lay[layer_idx].neu[neuron_idx].bias = layer_2_bs[neuron_idx];
    }
}
