#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

//#include "backprop.h"
#include "NN_utils.h"
#include "forward_pass.h"
#include "backprop.h"

#include "stdlib.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#if SETUP == XOR
    #define TRAIN_SIZE      4
    #define TEST_SIZE       4
    #define EPOCHS          10000
    #define LEARNING_RATE   0.15

    float train_samples[TRAIN_SIZE][INPUT_FEATURES] =
    {
        {0, 0},
        {0, 1},
        {1, 0},
        {1, 1}
    };
    float train_labels[TRAIN_SIZE] = { 0, 1, 1, 0 };

    float test_samples[TEST_SIZE][INPUT_FEATURES] =
    {
        {0, 0},
        {0, 1},
        {1, 0},
        {1, 1}
    };
    float test_labels[TEST_SIZE] = { 0, 1, 1, 0 };

#elif SETUP == MNIST
    #include "train_data_MNIST.h"
    #include "test_data_MNIST.h"

    #define TRAIN_SIZE      14700
    #define TEST_SIZE       6300
    #define EPOCHS          10
    #define LEARNING_RATE   0.01

#elif SETUP == PRIVATE_DATASET
    #include "train_data_PD.h"
    #include "test_data_PD.h"

    #define TRAIN_SIZE      4032
    #define TEST_SIZE       1728
    #define EPOCHS          20
    #define LEARNING_RATE   0.01

#endif

const activation_functions[N_LAYERS-1] = ACT_FUNCTIONS;

///////////////////////////////////////////////////
// redefinir o modelo e testar com dataset gerado
// introduzir fun��es de ativa��o novas
///////////////////////////////////////////////////


int main()
{
    layer *lay = NULL;
    int num_neurons[N_LAYERS] = LAYERS_SIZE;

    // Initialize the neural network
    if(init(&lay, N_LAYERS, num_neurons) != SUCCESS_INIT)
    {
        printf("Error in Initialization...\n");
        exit(0);
    }
    
    float accuracy = 0.0;

    //printf("||  Feeding input sample  ||\n");
    //feed_input(lay, train_samples, INPUT_FEATURES, 0);
    //printf("||  Forward pass  ||\n");
    //forward_prop(lay, num_neurons);
   


    //printf("||  Testing neural network  ||\n");
    //accuracy =  test_nn(lay, num_neurons, activation_functions, test_samples, test_labels, TEST_SIZE);
    //printf("Accuracy: %f\n\n", accuracy);
    //
    //printf("Training neural network...\n");
    //train_nn(lay, num_neurons, activation_functions, LEARNING_RATE, EPOCHS, train_samples, train_labels, TEST_SIZE);
    //printf("Train completed...\n\n");
    //
    //printf("Testing neural network...\n");
    //accuracy = test_nn(lay, num_neurons, activation_functions, test_samples, test_labels, TEST_SIZE);
    //printf("Test completed...\n");
    //printf("Accuracy: %f\n", accuracy);

    for (int run = 0; run < EPOCHS; run++)
    {
        clock_t begin = clock();
        //accuracy = test_nn(lay, num_neurons, activation_functions, test_samples, test_labels, TEST_SIZE);
        //printf("Accuracy: %f", accuracy);
        train_nn(lay, num_neurons, activation_functions, LEARNING_RATE, 1, train_samples, train_labels, TRAIN_SIZE);
        accuracy = test_nn(lay, num_neurons, activation_functions, test_samples, test_labels, TEST_SIZE);
        printf("\t\tAccuracy: %f", accuracy);
        clock_t end = clock();
        printf("\t\tElapsed: %f seconds\n", (double)(end - begin) / CLOCKS_PER_SEC);
    }
/*
    // Load training dataset
    float **train_samples = (float**) malloc(TRAIN_SIZE * sizeof(float*));
    float **train_labels = (float**) malloc(TRAIN_SIZE* sizeof(float*));

    load_dataset(train_samples, train_labels, TRAIN_SIZE, num_neurons[0]);            //REVIEW


    // Load testing dataset
    float **test_samples = (float**) malloc(TEST_SIZE * sizeof(float*));
    float **test_labels = (float**) malloc(TEST_SIZE* sizeof(float*));

    load_dataset(test_samples, test_labels, TRAIN_SIZE, num_neurons[0]);            //REVIEW
    
    
    // Train the neural network for E epochs
    for(int epoch=0; epoch<EPOCHS; epoch++)
    {
        train_neural_net();
        test_nn();
    }
*/

    return 0;
}



// Init model
