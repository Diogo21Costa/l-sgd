#ifndef FORWPROP_H
#define FORWPROP_H

#include "layer.h"
#include "NN_utils.h"

void feed_input(layer *in_layer, float **sample, int input_size, int sample_idx);
void forward_prop(layer* lay, int* layers_dims, int* activation_functions);
float node_activation_function(float neuron_input, int act_func);
float test_nn(layer* lay, int* layers_dims, int* activation_functions, float **samples, float *labels, int n_samples);


#endif