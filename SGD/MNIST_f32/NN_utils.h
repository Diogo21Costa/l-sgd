#ifndef NN_UTILS_H
#define NN_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include "layer.h"

// Loss Functions
#define MSE 0
#define BCE 1
#define CE  2

// Activation Functions
#define RELU    0
#define SIGMOID 1
#define TANH    2

#define SETUP	MNIST	
#define XOR				1
#define MNIST			2
#define PRIVATE_DATASET	3

// Model setup
#if SETUP == XOR
	#define LOAD_WEIGHTS    0
	#define N_LAYERS        4
	#define INPUT_FEATURES  2
	#define OUTPUT_FEATURES 1
	#define LAYERS_SIZE     {INPUT_FEATURES, 4, 4, OUTPUT_FEATURES}
	#define LOSS_FUNCTION   MSE
	#define ACT_FUNCTIONS   {RELU, RELU, SIGMOID} //{TANH, TANH, SIGMOID}
	float layer_0_wt[1][1], layer_1_wt[1][1], layer_2_wt[1][1];
	float layer_0_bs[1], layer_1_bs[1], layer_2_bs[1];

#elif SETUP == MNIST
	#define LOAD_WEIGHTS    1
	#define N_LAYERS        4
	#define INPUT_FEATURES  784
	#define OUTPUT_FEATURES 10
	#define LAYERS_SIZE     {INPUT_FEATURES, 40, 32, OUTPUT_FEATURES}
	#define LOSS_FUNCTION   BCE
	#define ACT_FUNCTIONS   {TANH, TANH, SIGMOID}

	#ifndef MODELPARAMS_MNIST_H
		#include "model_parameters_MNIST.h"
	#endif // !MODELPARAMS_MNIST_H

#elif SETUP == PRIVATE_DATASET
	#define LOAD_WEIGHTS    1
	#define N_LAYERS        4
	#define INPUT_FEATURES  6
	#define OUTPUT_FEATURES 1
	#define LAYERS_SIZE     {INPUT_FEATURES, 40, 32, OUTPUT_FEATURES}
	#define LOSS_FUNCTION   BCE
	#define ACT_FUNCTIONS   {TANH, TANH, SIGMOID}

	#ifndef MODELPARAMS_PD_H
		#include "model_parameters_PD.h"
	#endif // !MODELPARAMS_PD_H

#endif // SETUP




// Errors
#define SUCCESS_INIT 0
#define ERR_INIT 1
#define SUCCESS_DINIT 0
#define ERR_DINIT 1
#define SUCCESS_INIT_WEIGHTS 0
#define ERR_INIT_WEIGHTS 1
#define SUCCESS_UPDATE_WEIGHTS 0
#define SUCCESS_CREATE_ARCHITECTURE 0
#define ERR_CREATE_ARCHITECTURE 1
#define SUCCESS_LOAD_DATA   0
#define ERR_LOAD_DATA   1

//const int activation_functions[] = ACT_FUNCTIONS;
//const int layers_n_neurons[] = LAYERS_SIZE;

int load_dataset(float **in_samples, float** out_labels, int dataset_size, int input_dim);
int init(layer **lay, int num_layers, int* layers_dims);
int create_architecture(layer *lay, int num_layers, int* layers_dims);
int initialize_weights(layer* lay, int num_layers, int* layers_dims);

#endif
