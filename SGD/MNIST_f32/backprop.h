#ifndef BACKPROP_H
#define BACKPROP_H

#include "layer.h"
#include "forward_pass.h"
#include "NN_utils.h"

void train_nn(layer* lay, int* num_neurons, int* activation_functions,
            float lrn_rate, int epochs, 
            float **samples, float *labels, int n_samples);

void back_prop(layer* lay, int* layers_n_neurons, int* activation_functions, float* target, int loss_func);
void update_weights(layer* lay, float lrn_rate, int* layers_dims);
float activation_derivative(int activation_function, float neuron_output);
float loss_derivative(int loss_function, float out_target, float neuron_output);

#endif



